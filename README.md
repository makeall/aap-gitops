# Very basic example for GitOps controlled AAP Automation

## Create Runner Image for Gitlab Pipeline
Use ansible-builder to create an image based on the official AAP minimal Execution Environment image. Add the collections `ansible.controller` and `infra.controller_configuration`.

* See repo https://gitlab.com/aap-gitops/aap-gitops-runner-image for details
* Make image only privately available e.g. in quay.io

## Create Gitlab Pipeline
Set the following variables in `Settings->CICD`:
```
CONTROLLER_HOST: https://ansibl*****.com
CONTROLLER_PASSWORD: <password>
CONTROLLER_USERNAME: admin
CONTROLLER_VERIFY_SSL: false
DOCKER_AUTH_CONFIG: for private registry, get from quay.io as Docker configuration JSON
```

Create the pipeline, a basic example is in https://gitlab.com/aap-gitops/aap-gitops/-/blob/main/.gitlab-ci.yml

To pull the runner image from a private registry in Quay, create a robot read-only account in quay and add it as "Docker configuration" in Gitlab CICD variable `DOCKER_AUTH_CONFIG` 

## Create vars file with AAP objects
A very basic example is here: https://gitlab.com/aap-gitops/aap-gitops/-/blob/main/configs/controller_configs.yml

More examples are e.g. here: https://github.com/nesanton/ansible_tower_gitops/tree/main/example_repositories


